/*
    I N C L U D E S
*/
#include "SttStep.hpp"

#include "trcerr_e.h"

/*
    S T A T I C   G L O B A L   V A R I A B L E S
*/
static CHAR szSegG[] = "$SEG$ 990518 1000 STTSTEP.CPP";
/*
    D E F I N E S
*/


/*
    D E F I N E S
*/

/*
    I N T E R N A L   C L A S S E S
*/
ParameterList::ParameterList(
    PCHAR szParameter,
    CHAR chSeparator,
    BOOL fRemoveSpaces)
{
    CCStringObject *pcoString;
    CHAR *pchStart;
    CHAR *pchPos;
    SHORT sParLen;

    /*
        Scan parameters
    */
	pchStart = szParameter; 
    while (*pchStart)
    {
        sParLen = 0;
        pchPos = pchStart;

        while (*pchPos &&
            (*pchPos != chSeparator))
        {
            sParLen++;
            pchPos++;
        }
        if (*pchPos)
        {
            pchPos++;
        }
        /*
            Got a parameter, store it
        */
        if (fRemoveSpaces)
        {
            while (*pchStart == ' ')
            {
                pchStart++;
                sParLen--;
            }
            while ((sParLen > 0) && (pchStart[sParLen - 1] == ' '))
            {
                sParLen--;
            }
        }
        pcoString = new CCStringObject(pchStart, sParLen);
        coaParamM.Add(pcoString);
        /*
            Skip parameter and seperator
        */
        pchStart = pchPos;
    }
}

/* Header
Name        : ParameterList::~ParameterList
Description : Destructor of class ParameterList
Parameters  : -
Return      : -
*/
ParameterList::~ParameterList()
{
    CCStringObject *pcoString;

    while (coaParamM.GetSize())
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(0);

        delete pcoString;

        coaParamM.RemoveAt(0);
    }
}

/* Header
Name        : ParameterList::Param
Description : Returns one parameter
Parameters  : iIdx - parameter index
Return      : string pointer to the parameter with index iIdx
*/
PCHAR ParameterList::Param(
        int iIdx) const
{
    CCStringObject *pcoString;
    PCHAR pchRc = "";

    if ((iIdx >= 0) && (iIdx < coaParamM.GetSize()))
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(iIdx);
        pchRc = pcoString->buffer();
    }

    return (pchRc);
}

/*
    I M P L E M E N T A T I O N
    ===========================

    Methode InitStepList from class tmplTransactionFW
*/
VOID tmplTransactionFW::InitStepList()
{
    static BOOL fStepsAdded = FALSE;
    Step *pStep;
    SHORT sRc;

    TrcWritef(TRC_FUNC, "> tmplTransactionFW::InitStepList");
    /*
        Now create one instance of every step class and add it to the list
    */
    if (!fStepsAdded)
    {

	    pStep = new dcBarcode;
	    sRc = AddStep(pStep);

		pStep = new dcCardRead;
        sRc = AddStep(pStep);

        fStepsAdded = TRUE;
    }


	TrcWritef(TRC_FUNC, "< tmplTransactionFW::InitStepList");

    return;
}


PrtParam::PrtParam(
        PCHAR szParam)
{
    CCStringObject *pcoString;
    SHORT sPos;

    /*
        Scan parameters
    */
    while (szParam[0])
    {
        sPos = 0;

        while (szParam[sPos] &&
            (szParam[sPos] != '~'))
        {
            sPos++;
        }
        /*
            Got a parameter, store it
        */
        pcoString = new CCStringObject(szParam, sPos);
        coaParamM.Add(pcoString);
        /*
            Skip parameter and seperator
        */
        szParam += sPos;
        if (szParam[0] == '~')
        {
            szParam++;
        }
    }
}


PrtParam::~PrtParam()
{
    CCStringObject *pcoString;

    while (coaParamM.GetSize())
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(0);

        delete pcoString;

        coaParamM.RemoveAt(0);
    }
}


PCHAR PrtParam::Param(
        int iIdx) const
{
    CCStringObject *pcoString;
    PCHAR pchRc = "";

    if ((iIdx >= 0) && (iIdx < coaParamM.GetSize()))
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(iIdx);
        pchRc = pcoString->buffer();
    }

    return (pchRc);
}


SHORT PrtParam::GetSize() 
{
	SHORT sSize = (SHORT) coaParamM.GetSize();
	
	return(sSize);
}

DCStepParam::DCStepParam(PCHAR szParam)
{
    CCStringObject *pcoString;
    SHORT sLen;
    
    /*
        Scan parameters
    */
    while (szParam[0])
    {
        sLen = 1;

        while ((sLen < 3) && szParam[sLen])
        {
            sLen++;
        }
        /*
            Got a parameter, store it
        */
        pcoString = new CCStringObject(szParam, sLen);
        coaParamM.Add(pcoString);
        /*
            Skip parameter and seperator
        */
        szParam += sLen;
    }
}


DCStepParam::~DCStepParam()
{
    CCStringObject *pcoString;

    while (coaParamM.GetSize())
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(0);

        delete pcoString;

        coaParamM.RemoveAt(0);
    }
}


PCHAR DCStepParam::Param(int iIdx) const
{
    CCStringObject *pcoString;
    PCHAR pchRc = "000";

    if ((iIdx >= 0) && (iIdx < coaParamM.GetSize()))
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(iIdx);
        pchRc = pcoString->buffer();
    }

    return (pchRc);
}


SHORT DCStepParam::NumParam(int iIdx) const
{
    CHAR achTmp[6];
    SHORT sRc;

    strncpy(achTmp, Param(iIdx), sizeof(achTmp));
    for (sRc = 0 ; sRc < 3 ; sRc++)
    {
        switch (achTmp[sRc])
        {
            case '@':
                achTmp[sRc] = '0';
                break;

            case 'A':
                achTmp[sRc] = '1';
                break;

            case 'B':
                achTmp[sRc] = '2';
                break;

            case 'C':
                achTmp[sRc] = '3';
                break;

            case 'D':
                achTmp[sRc] = '4';
                break;

            case 'E':
                achTmp[sRc] = '5';
                break;

            case 'F':
                achTmp[sRc] = '6';
                break;

            case 'G':
                achTmp[sRc] = '7';
                break;

            case 'H':
                achTmp[sRc] = '8';
                break;

            case 'I':
                achTmp[sRc] = '9';
                break;

            default:    // Nothing to do
                break;
        }
    }
    achTmp[sRc] = 0;

    sRc = (SHORT)atoi(achTmp);

    return (sRc);
}

SHORT dcCardRead::Process( 
        PCHAR pchSectNextStep, 
        PCHAR szStepParams) 
{
	SHORT sRc;
	USHORT usLen;
	CHAR  achErr[3];
	CHAR  achCustErr[3];
	CHAR  achTrxErr[3];
	CHAR  achTimeErr[3];
	CHAR  achCustomer[4];
	TrcWritef(TRC_FUNC, ">  dcCardRead::Process ( %s )", szStepParams);

	sRc = VAR_FW.SetVar(VAR_NEW_CUSTOMER, "YES");

	/*
		Synchronize the error counter between CMOS and VARIABLE
	*/
	usLen = sizeof(achErr);
	sRc = PCMOS_FW.GetParm(&achErr[0], &usLen, CMOS_BARCODE_CUS_ERROR, DCCMOS_SECTION_NAME);
	sRc = VAR_FW.SetVar(VAR_ERR_CUST_COUNT, achErr);
	TrcWritef(TRC_INFO, "> dcCardRead VAR_ERR_CUST_COUNT: %d", achErr);
	
	sRc = PCMOS_FW.GetParm(&achErr[0], &usLen, CMOS_BARCODE_TRX_ERROR, DCCMOS_SECTION_NAME);
	sRc = VAR_FW.SetVar(VAR_ERR_TRX_COUNT, achErr);
	TrcWritef(TRC_INFO, "> dcCardRead VAR_ERR_TRX_COUNT: %d", achErr);

	sRc = VAR_FW.GetVar(VAR_NEW_CUSTOMER, achCustomer, sizeof(achCustomer));
	TrcWritef(TRC_INFO, "> dcCardRead BARCODE_NEW_CUSTOMER: %s", achCustomer);

	

	sRc = ProcessBaseStep(pchSectNextStep, FuncName(), szStepParams);

	TrcWritef(TRC_FUNC, ">  dcCardRead::exit %d -> next (%s)", sRc, pchSectNextStep);
	return sRc;
}

VOID dcBarcode::UpdateCounter(VOID)
{
	SHORT sRc;
	SHORT sMaxTimeout;
	SHORT sTrxErr;
	SHORT sCustErr;
	SHORT sTimeErr;
	USHORT usLen;
	CHAR  achCustErr[3];
	CHAR  achTrxErr[3];
	CHAR  achTimeErr[3];
	CHAR  achCustomer[4];

	TrcWritef(TRC_FUNC, "> dcBarcode::UpdateCounter()");

	sRc = DATA_FW.GetParm(&sMaxTimeout, DCBARCODE_MAX_TIMEOUT, DCBARCODE_SECTION);
	if(sRc != 0)
		sMaxTimeout=3;

	sRc = VAR_FW.GetVar(VAR_ERR_CUST_COUNT, achCustErr, sizeof(achCustErr));
	if (sRc)
		sCustErr = 0;
	else
		sCustErr = atoi(achCustErr);

	sRc = VAR_FW.GetVar(VAR_ERR_TRX_COUNT, achTrxErr, sizeof(achTrxErr));
	if (sRc)
		sTrxErr = 0;
	else
		sTrxErr = atoi(achTrxErr);

	usLen = sizeof(achTimeErr);
	sRc = PCMOS_FW.GetParm(&achTimeErr[0], &usLen, CMOS_BARCODE_TIME_ERROR, DCCMOS_SECTION_NAME);
	sTimeErr = atoi(achTimeErr);

	TrcWritef(TRC_INFO, "sTimeErr: %d\nMAX_TIMEOUT: %d\nBARCODE_CUST_ERR_COUNT: %d\nBARCODE_TRX_ERR_COUNT: %d", sTimeErr, sMaxTimeout, sCustErr, sTrxErr);

	sRc = VAR_FW.GetVar(VAR_NEW_CUSTOMER, achCustomer, sizeof(achCustomer));
	TrcWritef(TRC_INFO, "BARCODE_NEW_CUSTOMER: %s", achCustomer);

	if (strcmp(achCustomer, "YES")==0)
	{
		sRc = VAR_FW.SetVar(VAR_NEW_CUSTOMER, "NO");
		sCustErr++;
		if (sCustErr > 99)
			sCustErr = 0;
		sprintf(achCustErr, "%02d", sCustErr);
		sRc = VAR_FW.SetVar(VAR_ERR_CUST_COUNT, achCustErr);
		sRc = PCMOS_FW.PutParm(achCustErr, strlen(achCustErr) + 1, CMOS_BARCODE_CUS_ERROR, DCCMOS_SECTION_NAME);
		TrcWritef(TRC_INFO, "BARCODE_CUS_ERR now %d", sCustErr);

		sTimeErr++;
		sprintf(achTimeErr, "%02d", sTimeErr);
		sRc = PCMOS_FW.PutParm(achTimeErr, strlen(achTimeErr) + 1, CMOS_BARCODE_TIME_ERROR, DCCMOS_SECTION_NAME);
		TrcWritef(TRC_INFO, "BARCODE_TIME_ERR now %d", sTimeErr);
	}
	// Increment the transaction timeout counter
	sTrxErr++;
	if (sTrxErr > 99)
		sTrxErr = 0;
	sprintf(achTrxErr, "%02d", sTrxErr);
	sRc = VAR_FW.SetVar(VAR_ERR_TRX_COUNT, achTrxErr);
	sRc = PCMOS_FW.PutParm(achTrxErr, strlen(achTrxErr) + 1, CMOS_BARCODE_TRX_ERROR, DCCMOS_SECTION_NAME);
	TrcWritef(TRC_INFO, "BARCODE_TRX_ERR now %d", sTrxErr);

	if (sTimeErr >= sMaxTimeout)
	{
		TrcWritef(TRC_INFO, "sTimeErr: %d\nsMaxTimeout: %d\nSend Unsolicited Barcode Timeout", sTimeErr, sMaxTimeout);
		APPL_FW.TransactionHandlerRequest(T2H_UNSOL_BARCODE_TIMEOUT);
		// zeroes the three variables
		sCustErr = 0;
		sprintf(achCustErr, "%02d", sCustErr);
		sRc = VAR_FW.SetVar(VAR_ERR_CUST_COUNT, achCustErr);
		sRc = PCMOS_FW.PutParm(achCustErr, strlen(achCustErr) + 1, CMOS_BARCODE_CUS_ERROR, DCCMOS_SECTION_NAME);
		sTrxErr = 0;
		sprintf(achTrxErr, "%02d", sTrxErr);
		sRc = VAR_FW.SetVar(VAR_ERR_TRX_COUNT, achTrxErr);
		sRc = PCMOS_FW.PutParm(achTrxErr, strlen(achTrxErr) + 1, CMOS_BARCODE_TRX_ERROR, DCCMOS_SECTION_NAME);
		sTimeErr = 0;
		sprintf(achTimeErr, "%02d", sTimeErr);
		sRc = PCMOS_FW.PutParm(achTimeErr, strlen(achTimeErr) + 1, CMOS_BARCODE_TIME_ERROR, DCCMOS_SECTION_NAME);
		TrcWritef(TRC_INFO, "sCustErr: %d\nsTrxErr: %d\nsTimeErr: %d", sCustErr, sTrxErr, sTimeErr);
	}

	TrcWritef(TRC_FUNC, "< dcBarcode::UpdateCounter()");
}
SHORT dcBarcode::Process(
		   PCHAR pchSectNextStep,
		   PCHAR szStepParams)
{ 
    DCStepParam parTmp(szStepParams);
    SHORT	sRc,sRet;
	CHAR	chTemp[30];
	CHAR	szBuffer[256];
	SHORT	sBarcodeType=0;
	char	achTaxIdScr[4];
	char	achCustNoScr[4];
	char	achRefNoScr[4];
	char	achAmountScr[4];
	CHAR	achTimeErr[3];
    CCString			strClearScreen;
	BOOL				fDone = FALSE;
	CHAR				chCancelFDK;
	USHORT				usScanTimes = 1;
	USHORT				usScanTry = 1;
	SHORT				sTimer = 10;
	SHORT				sMaxScanError;
	SHORT				sScanError = 1;
	SHORT				sKeyboardTimer = 30;
	SHORT				sMoreTimeTimer = 19;
    TrcWritef(TRC_FUNC, "> dcBarcode::Process(%s, %s)",
                                            pchSectNextStep, szStepParams);


	strClearScreen = "";
	/*
		Get the screens number use by application
	*/
	sRet = DATA_FW.GetParm(achTaxIdScr, sizeof(achTaxIdScr), BARCODE_SCR_TAX_ID, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achTaxIdScr,	TAX_ID_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_TAX_ID, achTaxIdScr);

	sRet = DATA_FW.GetParm(achCustNoScr, sizeof(achCustNoScr), BARCODE_SCR_CUST_NO, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achCustNoScr, CUST_NO_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_CUST_NO, achCustNoScr);

	sRet = DATA_FW.GetParm(achRefNoScr, sizeof(achRefNoScr), BARCODE_SCR_REF_NO, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achRefNoScr, REF_NO_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_REF_NO, achRefNoScr);

	sRet = DATA_FW.GetParm(achAmountScr, sizeof(achAmountScr), BARCODE_SCR_AMOUNT, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achAmountScr, AMOUNT_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_AMOUNT, achAmountScr);

	sRc = DATA_FW.PutScreen(strClearScreen,BARCODE_SCR_TAX_ID,"SCREEN");
    sRc = DATA_FW.PutScreen(strClearScreen,BARCODE_SCR_CUST_NO,"SCREEN");
	sRc = DATA_FW.PutScreen(strClearScreen,BARCODE_SCR_REF_NO,"SCREEN");
	sRc = DATA_FW.PutScreen(strClearScreen,BARCODE_SCR_AMOUNT,"SCREEN");
	
	sRc = DATA_FW.GetParm(&sMaxScanError, BARCODE_MAX_SCAN_ERROR, DCBARCODE_SECTION);
	if(sRc != 0)
		sMaxScanError = 1;
	TrcWritef(TRC_INFO, " sRc = %d, %s = %d", sRc, BARCODE_MAX_SCAN_ERROR, sMaxScanError);

	sRet = DATA_FW.GetTimer(sKeyboardTimer, TIMER_KEYBOARD_RESPONSE,
                                                            TIMER_SECTION);
	sRet = DATA_FW.GetTimer(sMoreTimeTimer, TIMER_MORE_TIME,                                                     TIMER_SECTION);

	TrcWritef(TRC_INFO, " GetTimer sKeyboardTimer = %d,sMoreTimeTimer=%d,sRet=%d", sKeyboardTimer,sMoreTimeTimer, sRet);

	sRet = DATA_FW.PutTimer(0, TIMER_MORE_TIME,TIMER_SECTION);
	TrcWritef(TRC_INFO, " > PutTimer sMoreTimeTimer=%0,sRet=%d",sMoreTimeTimer, sRet);

	while(!fDone)
	{
		
		sRc = ProcessBaseStep(pchSectNextStep, FuncName(), szStepParams);
		
		TrcWritef(TRC_FUNC, "< dcBarcode::BaseProcess Return -> %d ",sRc);

		CCBcrFW	* barcodeFwM; 
		CCJournalFW * journalFwM;

		barcodeFwM = new CCBcrFW;
		journalFwM = new CCJournalFW;

		SHORT barcodeStatus;
		barcodeStatus = barcodeFwM->GetbStatus();
		TrcWritef(TRC_FUNC, "> BARCODE STATUS = (%d)",barcodeStatus);

		if(barcodeStatus == CCBCRFW_NOT_OPERATIONAL)
		{
				sRc = MMBARCODEFW_RC_ERROR;
				TrcWritef(TRC_FUNC, "> BARCODE DEVICE ERROR = (%d)",sRc);	
		}
		else
		{
			memset(szBuffer, 0, sizeof(szBuffer));
			sRet = VAR_FW.GetVar("DCMC_BCR_DATA", szBuffer, sizeof(szBuffer));
			TrcWritef(TRC_FUNC, "> DCMC_BCR_DATA = (%s)",szBuffer);
		
			sFlagTypeM = atoi(parTmp.Param(6));
			sTxnTypeM = atoi(parTmp.Param(7));
			
			TrcWritef(TRC_INFO, "sFlagTypeM: %d", sFlagTypeM);
			TrcWritef(TRC_DETAIL, "szBuffer:%s", szBuffer);
			if(strcmp(szBuffer,"") != 0 )
			{
				TrcWritef(TRC_INFO, "Reset Timeout counter");
				memset(achTimeErr, 0, sizeof(achTimeErr));
				sprintf(achTimeErr, "00");
				sRet = PCMOS_FW.PutParm(achTimeErr, strlen(achTimeErr) + 1, CMOS_BARCODE_TIME_ERROR, DCCMOS_SECTION_NAME);	
			}
			switch (sFlagTypeM)
			{
				case DCBARCODE_FLAG_ALL:
					do 
					{
						sRc = FormatBarcode(sBarcodeType, szBuffer);
						sBarcodeType++;
					} while (sRc && (sBarcodeType <= DCBARCODE_TYPE_WATER2));
					break;

				case DCBARCODE_FLAG_SCB:
					sRc = FormatBarcode(DCBARCODE_TYPE_SCB, szBuffer); 
					break;
				case DCBARCODE_FLAG_WATER1:
					sRc = FormatBarcode(DCBARCODE_TYPE_WATER1, szBuffer); 
					break;
				case DCBARCODE_FLAG_WATER2:
					sRc = FormatBarcode(DCBARCODE_TYPE_WATER2, szBuffer); 
					break;
			}
				
		}
		if(sRc != MMBARCODEFW_RC_ERROR)
		{
			if ((strcmp(pchSectNextStep,parTmp.Param(1))==0) || (strcmp(pchSectNextStep,parTmp.Param(2))==0))
			{
				if (strcmp(pchSectNextStep,parTmp.Param(1))== 0)
				{
					
					sRc = MMBARCODEFW_RC_TIMEOUT;
					TrcWritef(TRC_FUNC, "> BARCODE DEVICE TIMEOUT = (%d)",sRc);//
					
				}

				if (strcmp(pchSectNextStep,parTmp.Param(2))==0)
				{
					sRc = MMBARCODEFW_RC_CANCELLED;
					
					TrcWritef(TRC_FUNC, "> BARCODE DEVICE CANCEL = (%d)",sRc); //
				}
				
			}
		}
		
		switch (sRc)
		{	
			case MMBARCODEFW_RC_OK:		
				strcpy(pchSectNextStep, parTmp.Param(3));
				fDone = TRUE;
				break;
			case MMBARCODEFW_RC_ERROR:

				APPL_FW.TransactionHandlerRequest(T2H_UNSOL_BARCODE_ERR);
				journalFwM->Write(DCJOU_BCR_OFFLINE);
				strcpy(pchSectNextStep, parTmp.Param(4));
				fDone = TRUE;
				
				break;
			case MMBARCODEFW_RC_INVALID:
				if (sScanError < sMaxScanError)
				{
					sScanError++;
					TrcWritef(TRC_FUNC, "> BARCODE INVALID FORMAT COUNT = (%d)",sScanError); //
				}
				else
				{
					strcpy(pchSectNextStep, parTmp.Param(1));
					fDone = TRUE;
				}

				break;
			case MMBARCODEFW_RC_TIMEOUT:
				/*if (usScanTry >= usScanTimes)
				{
					UpdateCounter();
					strcpy(pchSectNextStep, parTmp.Param(1));// Timeout
					fDone = TRUE;
				}*/
				UpdateCounter();
				strcpy(pchSectNextStep, parTmp.Param(1)); //Timeout
				fDone = TRUE;
				break;
			case MMBARCODEFW_RC_CANCELLED:
				strcpy(pchSectNextStep, parTmp.Param(2)); //Cancel
				fDone = TRUE;
				break;
		}
		
	}
	  
	sRet = DATA_FW.PutTimer(sMoreTimeTimer, TIMER_MORE_TIME,TIMER_SECTION);
	TrcWritef(TRC_INFO, " > Use original Timer sMoreTimeTimer=%d,sRet=%d", sMoreTimeTimer, sRet);
	sRc = CCTAFW_RC_JUMP_LABEL;

    TrcWritef(TRC_FUNC, "< dcBarcode::Process -> %d, next %s",
                                                    sRc, pchSectNextStep);
    return (sRc);
}

SHORT dcBarcode::FormatBarcode(SHORT sBarcodeType, PCHAR pchRawData)
{
	SHORT sRc = 0;
	SHORT sRet;
	SHORT sTaxIDLen = 15;
	SHORT sRefNo1Len = 18;
	SHORT sRefNo2Len = 18;
	SHORT sRefNo1Pos = 15;
	SHORT sRefNo2Pos = 1;
	SHORT sBarcodeTypePos = 23;
	SHORT sPart1Len;
	SHORT sPart2Len;
	BOOL  fBarcodeType = TRUE;
	BOOL  fSetAmountBuffer = TRUE;
	char achBufferB[33];
	char achBufferC[33];
	char achTmp[256];
	
	char achAmount[20];
	char achBuf1[10];
	char achBuf2[10];
	char achTaxId [16];
	char achRefNo1[21];
	char achRefNo2[21];
	int	 length = 0;
	char achTaxIdScr[4];
	char achCustNoScr[4];
	char achRefNoScr[4];
	char achAmountScr[4];
	char achDueDate[7];
	char achBranchID[10];
	char achUserID[8];
	SHORT sParm01;
	SHORT sAmtBufLen;
	char achBarcodeType[1];
	
	double dbAmount;
	char achScreenAmount[20];

	TrcWritef(TRC_FUNC, "> dcBarcode::FormatBarcode (%d, %s)\n", sBarcodeType, pchRawData);

	//separate Barcode
	ParameterList parList(pchRawData,' ');

	sRet = DATA_FW.GetParm(achTaxIdScr, sizeof(achTaxIdScr), BARCODE_SCR_TAX_ID, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achTaxIdScr,	TAX_ID_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_TAX_ID, achTaxIdScr);

	sRet = DATA_FW.GetParm(achCustNoScr, sizeof(achCustNoScr), BARCODE_SCR_CUST_NO, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achCustNoScr, CUST_NO_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_CUST_NO, achCustNoScr);

	sRet = DATA_FW.GetParm(achRefNoScr, sizeof(achRefNoScr), BARCODE_SCR_REF_NO, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achRefNoScr, REF_NO_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_REF_NO, achRefNoScr);

	sRet = DATA_FW.GetParm(achAmountScr, sizeof(achAmountScr), BARCODE_SCR_AMOUNT, DCBARCODE_SECTION);
	if(sRet != 0)
		strcpy(achAmountScr, AMOUNT_SCR);
	TrcWritef(TRC_INFO, " sRet = %d, %s = %s", sRet, BARCODE_SCR_AMOUNT, achAmountScr);

	/*
		Get the format parameters
	*/
	sRet = DATA_FW.GetParm(&sTaxIDLen, DCBARCODE_TAXID_LEN, DCBARCODE_SECTION);
	if(sRet != 0)
		sTaxIDLen=15;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_TAXID_LEN, sTaxIDLen);

	sRet = DATA_FW.GetParm(&sRefNo1Len, DCBARCODE_REFNO1_LEN, DCBARCODE_SECTION);
	if(sRet != 0)
		sRefNo1Len=18;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_REFNO1_LEN, sRefNo1Len);

	sRet = DATA_FW.GetParm(&sRefNo2Len, DCBARCODE_REFNO2_LEN, DCBARCODE_SECTION);
	if(sRet != 0)
		sRefNo2Len=18;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_REFNO2_LEN, sRefNo2Len);

	sRet = DATA_FW.GetParm(&sRefNo1Pos, DCBARCODE_REFNO1_POS, DCBARCODE_SECTION);
	if(sRet != 0)
		sRefNo1Pos=15;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_REFNO1_POS, sRefNo1Pos);

	sRet = DATA_FW.GetParm(&sRefNo2Pos, DCBARCODE_REFNO2_POS, DCBARCODE_SECTION);
	if(sRet != 0)
		sRefNo2Pos=1;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_REFNO2_POS, sRefNo2Pos);

	sRet = DATA_FW.GetParm(&fBarcodeType, DCBARCODE_TYPE_FLAG, DCBARCODE_SECTION);
	if(sRet != 0)
		fBarcodeType=TRUE;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_TYPE_FLAG, fBarcodeType);

	sRet = DATA_FW.GetParm(&sBarcodeTypePos, DCBARCODE_TYPE_POS, DCBARCODE_SECTION);
	if(sRet != 0)
		sBarcodeTypePos=23;
	TrcWritef(TRC_INFO, " sRet = %d, %s = %d", sRet, DCBARCODE_TYPE_POS, sBarcodeTypePos);
	/*
		Clear Reference No
	*/
	memset(achRefNo1, 0, sizeof(achRefNo1));
	memset(achRefNo2, 0, sizeof(achRefNo2));

	if(parList.Param(0)[0] == 's') // clear s from first character
	{
		memset(achTmp,0,sizeof(achTmp));
		memcpy(achTmp,&parList.Param(0)[1],strlen(parList.Param(0))-1);
		strcpy(parList.Param(0),achTmp);
	}



	switch (sBarcodeType)
	{

		case DCBARCODE_TYPE_GENERAL:
		
			TrcWritef(TRC_FUNC, "Parameter size = %d ",parList.Size());
			//barcode field 3 or 4 allowed
			if((parList.Size() < 3) || (parList.Size() > 4))
			{
				sRc = MMBARCODEFW_RC_INVALID;
				TrcWritef(TRC_FUNC, "MMBARCODEFW_RC_INVALID Size");

				break;
			}

			if(parList.Param(0)[0] != '|')
			{
				sRc = MMBARCODEFW_RC_INVALID;
				TrcWritef(TRC_FUNC, "MMBARCODEFW_RC_INVALID |");
				break;
			}

			//check if there is line feed in the barcode
			// if there is LF then remove it
			if(parList.Param(1)[0] == 0x0A)
			{
				memset(achTmp,0,sizeof(achTmp));
				memcpy(achTmp,&parList.Param(1)[1],strlen(parList.Param(1))-1);
				strcpy(parList.Param(1),achTmp);
			}
			if(parList.Param(2)[0] == 0x0A)
			{
				memset(achTmp,0,sizeof(achTmp));
				memcpy(achTmp,&parList.Param(2)[1],strlen(parList.Param(2))-1);
				strcpy(parList.Param(2),achTmp);
			}
			if(parList.Size() == 4)
			{
				if(parList.Param(3)[0] == 0x0A)
				{
					memset(achTmp,0,sizeof(achTmp));
					memcpy(achTmp,&parList.Param(3)[1],strlen(parList.Param(3))-1);
					strcpy(parList.Param(3),achTmp);
				}
			}
			
			memset(achTmp,0,sizeof(achTmp));
			memcpy(achTmp,&parList.Param(0)[1],strlen(parList.Param(0))-1);
	
			// Update Tax Id screen
			sRet = DATA_FW.PutScreen(achTmp,strlen(achTmp),achTaxIdScr, SCREEN_SECTION);
			TrcWritef(TRC_FUNC, "PutScreen Tax Id screen=(%s,%d)",achTaxIdScr,sRet);
			// Update Customer No screen
			sRet = DATA_FW.PutScreen(parList.Param(1),strlen(parList.Param(1)),achCustNoScr, SCREEN_SECTION);
			TrcWritef(TRC_FUNC, "PutScreen Customer No screen=(%s,%d)",achCustNoScr,sRet);
			// Update Reference No screen
			sRet = DATA_FW.PutScreen(parList.Param(2),strlen(parList.Param(2)),achRefNoScr, SCREEN_SECTION);
			TrcWritef(TRC_FUNC, "PutScreen Reference No screen=(%s,%d)",achRefNoScr,sRet);
			// Update Amount screen
			if(parList.Size() == 4)//if there is amount
			{
				//format amount with 2 decimal

				memset(achBuf1,0,sizeof(achBuf1));
				memset(achBuf2,0,sizeof(achBuf2));
				memset(achTmp,0,sizeof(achTmp));
				memset(achAmount,0,sizeof(achAmount));
				strcpy(achAmount, parList.Param(3));
				TrcWritef(TRC_FUNC, " Amount = %s",	achAmount);
				length = strlen(achAmount) - 2;
				if(length > 0)
				{
					memcpy(achBuf1,&parList.Param(3)[0],length);
					memcpy(achBuf2,&parList.Param(3)[length],2);
					strcpy(achTmp,achBuf1);
					strcat(achTmp,".");
					strcat(achTmp,achBuf2);
				}
				else
					sprintf(achTmp,"0.%02d",atoi(parList.Param(3)));

				//Update by Farm  26/03/2014
				dbAmount = atof(achTmp);
	
				sprintf(achScreenAmount,"%.2f",dbAmount);

				TrcWritef(TRC_FUNC,"Farm New Function Screen Amount = [%s]", achScreenAmount);
				
				sRet = DATA_FW.PutScreen(achScreenAmount,strlen(achScreenAmount),achAmountScr, SCREEN_SECTION);

				//Update by Farm  26/03/2014

//				Old Amount				
//				sRet = DATA_FW.PutScreen(achTmp,strlen(achTmp),achAmountScr, SCREEN_SECTION);
				TrcWritef(TRC_FUNC, "PutScreen Amount screen=(%s,%d)",achAmountScr,sRet);
				
				length = strlen(achAmount);
				sprintf(achTmp, "%s01", PARAMETER_KEY_PREFIX);
				sRet = DATA_FW.GetParm(&sParm01, achTmp, PARAMETER_SECTION);
				if (sParm01 & 8)
					sAmtBufLen = 12;
				else
					sAmtBufLen = 8;

				memset(achTmp, 0, sizeof(achTmp));
				if(length < sAmtBufLen)
				{
					for(int i = 0; i < sAmtBufLen - length;i++)
					{
						strcpy(achTmp,achAmount);
						sprintf(achAmount,"0%s",achTmp);						
					}
				}

			}
			else
				fSetAmountBuffer = FALSE;
		
#ifdef OLD_METHOD
			//put in buffer B & C

			memset(achTaxId,0,sizeof(achTaxId));
			length = strlen(achTemp);
			if(length < sTaxIDLen)
			{
				memset(achTaxId,' ',sTaxIDLen);
				memcpy(achTaxId,achTemp,length);
			}
			else if (length == sTaxIDLen)
				strcpy(achTaxId,achTemp);
			

			memset(achTemp,0,sizeof(achTemp));
			strcpy(achTemp,parList.Param(1));
			memset(achCustomerNo1,0,sizeof(achCustomerNo1));
			memset(achCustomerNo2,0,sizeof(achCustomerNo2));
			length = strlen(achTemp);
			if(length == 15)
			{
				strcpy(achCustomerNo1,achTemp);
				strcpy(achCustomerNo2,"   ");
			}
			else if (length < 15)
			{
				strcpy(achCustomerNo1,achTemp);
				for(int i = 0; i < 15-length; i++)
					strcat(achCustomerNo1," ");
				strcpy(achCustomerNo2,"   ");
			}
			else
			{
				memcpy(achCustomerNo1,&achTemp[0],15);
				memcpy(achCustomerNo2,&achTemp[15],3);
			}
			strcat(achCustomerNo2, "  ");

			memset(achTemp,0,sizeof(achTemp));
			strcpy(achTemp, parList.Param(2));
			memset(achRefNo,0,sizeof(achRefNo));
			length = strlen(achTemp);
			strcpy(achRefNo,achTemp);
			if(length < 18)
			{	
				for(int i = 0; i < 18-length; i++)
					strcat(achRefNo," ");
			}
			strcat(achRefNo, "1        ");

			if(parList.Size() == 4)//if there is amount
			{
				memset(achTemp,0, sizeof(achTemp));
				strcpy(achTemp, parList.Param(3));
				memset(achAmount,0,sizeof(achAmount));
				strcpy(achAmount,achTemp);
				length = strlen(achTemp);
				TrcWritef(TRC_INFO, " Amount = %s",
									achAmount);
				if(length < 12)
				{
					for(int i = 0; i < 12 - length;i++)
					{
						sprintf(achAmount,"0%s",achTemp);
						strcpy(achTemp,achAmount);
					}
				}
			}

			TrcWritef(TRC_INFO, " TaxId = \"%s\", CustomerNo1 = \"%s\", CustomerNo2 = \"%s\", RefNo = \"%s\", Amount = \"%s\""
								, achTaxId,achCustomerNo1,achCustomerNo2,achRefNo,achAmount);

			memset(achTmp,0,sizeof(achTmp));
			strcpy(achTmp,achTaxId);
			strcat(achTmp,achCustomerNo1);
			memset(achTemp,0,sizeof(achTemp));
			strcpy(achTemp,achCustomerNo2);
			strcat(achTemp,achRefNo);
			if(parList.Size() == 4)//if there is amount
			{
				//strcat(achTemp,achAmount);
				sRet = VAR_FW.SetVar(DCVAR_AMOUNT_BUF, achAmount);
			}
			sRet = VAR_FW.SetVar(DCVAR_GEN_B_BUF, achTmp);
			sRet = VAR_FW.SetVar(DCVAR_GEN_C_BUF, achTemp);
#else
			memset(achTmp,0,sizeof(achTmp));
			memcpy(achTmp,&parList.Param(0)[1],strlen(parList.Param(0))-1);
			memset(achTaxId,0,sizeof(achTaxId));
			length = strlen(achTmp);
			if(length > sTaxIDLen)
				memcpy(achTaxId,achTmp,sTaxIDLen);
			else 
				memcpy(achTaxId,achTmp,length);

			memset(achTmp,0,sizeof(achTmp));
			strcpy(achTmp, parList.Param(1));
			length = strlen(achTmp);
			if(length > sRefNo1Len)
				memcpy(achRefNo1,achTmp,sRefNo1Len);
			else 
				memcpy(achRefNo1,achTmp,length);
			
			memset(achTmp,0,sizeof(achTmp));
			strcpy(achTmp, parList.Param(2));
			length = strlen(achTmp);
			if(length > sRefNo2Len)
				memcpy(achRefNo2,achTmp,sRefNo2Len);
			else 
				memcpy(achRefNo2,achTmp,length);


#endif
			achBarcodeType[0] = '3';
			TrcWritef(TRC_FUNC, "Set achBarcodeType =%c\n",achBarcodeType[0]);
			break;

		case DCBARCODE_TYPE_SCB:
			if(strlen(pchRawData)!=65)
			{
				sRc = MMBARCODEFW_RC_INVALID;
				break;
			}

			memset(achTaxId,0,sizeof(achTaxId));
			memset(achRefNo1,0,sizeof(achRefNo1));
			memset(achRefNo2,0,sizeof(achRefNo2));
			memset(achAmount,0,sizeof(achAmount));

			memcpy(achTaxId,&pchRawData[0],12);
			memcpy(achRefNo1,&pchRawData[12],20);
			memcpy(achRefNo2,&pchRawData[32],20);
			memcpy(achAmount,&pchRawData[53],12);
		
			if(sTxnTypeM == 1)//if telephone bill
			{
				memcpy(achRefNo1, achRefNo2, strlen(achRefNo2));
				memcpy(achRefNo2,&pchRawData[13],19);
			}
			else if(sTxnTypeM == 2) //if credit card
			{
				memcpy(achRefNo1,&pchRawData[16],16);
			}

			// Update Tax ID screen
			sRet = DATA_FW.PutScreen(achTaxId,strlen(achTaxId),achTaxIdScr, SCREEN_SECTION);

			// Update Customer No screen
			sRet = DATA_FW.PutScreen(achRefNo1,strlen(achRefNo1),achCustNoScr, SCREEN_SECTION);
		
			// Update Reference No screen
			sRet = DATA_FW.PutScreen(achRefNo2,strlen(achRefNo2),achRefNoScr, SCREEN_SECTION);

			// Format Amount with 2 decimal

			memset(achBuf1,0,sizeof(achBuf1));
			memset(achBuf2,0,sizeof(achBuf2));
			length = strlen(achAmount) - 2;
			if(length > 0)
			{
				memcpy(achBuf1,&achAmount[0],length);
				memcpy(achBuf2,&achAmount[length],2);
				memset(achTmp,0,sizeof(achTmp));
				sprintf(achTmp,"%d.%02d",atoi(achBuf1),atoi(achBuf2));
			}
			else
				sprintf(achTmp,"0.%02d",atoi(achTmp));
		
			sRet = DATA_FW.PutScreen(achTmp,strlen(achTmp),achAmountScr, SCREEN_SECTION);

			achBarcodeType[0] = '2';
			TrcWritef(TRC_FUNC, "Set achBarcodeType =%c",achBarcodeType[0]);
			break;

		case DCBARCODE_TYPE_WATER1:
			if(strlen(pchRawData)!=36)
			{
				sRc = MMBARCODEFW_RC_INVALID;
				break;
			}
			memset(achTaxId,0,sizeof(achTaxId));
			memset(achRefNo1,0,sizeof(achRefNo1));
			memset(achRefNo2,0,sizeof(achRefNo2));
			memset(achDueDate,0,sizeof(achDueDate));
			memset(achAmount,0,sizeof(achAmount));

			strcpy(achTaxId,"410203052101");			// Constant value for Water Type 1
			memcpy(achRefNo1,&pchRawData[0],13);
			memcpy(achRefNo2,&pchRawData[13],7);
			memcpy(achDueDate,&pchRawData[30],6);
			strcat(achRefNo2,achDueDate);
			memcpy(achAmount,&pchRawData[20],10);

			// Update Tax ID screen
			sRet = DATA_FW.PutScreen(achTaxId,strlen(achTaxId),achTaxIdScr, SCREEN_SECTION);

			// Update Customer No screen
			sRet = DATA_FW.PutScreen(achRefNo1,strlen(achRefNo1),achCustNoScr, SCREEN_SECTION);
		
			// Update Reference No screen
			sRet = DATA_FW.PutScreen(achRefNo2,strlen(achRefNo2),achRefNoScr, SCREEN_SECTION);

			// Format Amount with 2 decimal

			memset(achBuf1,0,sizeof(achBuf1));
			memset(achBuf2,0,sizeof(achBuf2));
			length = strlen(achAmount) - 2;
			if(length > 0)
			{
				memcpy(achBuf1,&achAmount[0],length);
				memcpy(achBuf2,&achAmount[length],2);
				memset(achTmp,0,sizeof(achTmp));
				sprintf(achTmp,"%d.%02d",atoi(achBuf1),atoi(achBuf2));
			}
			else
				sprintf(achTmp,"0.%02d",atoi(achAmount));
		
			sRet = DATA_FW.PutScreen(achTmp,strlen(achTmp),achAmountScr, SCREEN_SECTION);

			/*
				Add 2 leading zeroes to the Amount
			*/
			memset(achTmp, 0, sizeof(achTmp));
			strcpy(achTmp, "00");
			strcat(achTmp, achAmount);
			strcpy(achAmount, achTmp);
			
			achBarcodeType[0] = '1';
			TrcWritef(TRC_FUNC, "Set achBarcodeType =%c",achBarcodeType[0]);
			break;

		case DCBARCODE_TYPE_WATER2:
			if(strlen(pchRawData)!=41)
			{
				sRc = MMBARCODEFW_RC_INVALID;
				break;
			}
			memset(achTaxId,0,sizeof(achTaxId));
			memset(achRefNo1,0,sizeof(achRefNo1));
			memset(achRefNo2,0,sizeof(achRefNo2));
			memset(achDueDate,0,sizeof(achDueDate));
			memset(achUserID,0,sizeof(achUserID));
			memset(achBranchID,0,sizeof(achBranchID));
			memset(achAmount,0,sizeof(achAmount));

			strcpy(achTaxId,"410200016601");// Constant value for Water Type 2
			memcpy(achUserID, &pchRawData[10], 7);
			memcpy(achBranchID,&pchRawData[17],9);
			strcpy(achRefNo1,achBranchID);
			strcat(achRefNo1,achUserID);
			memcpy(achRefNo2,&pchRawData[0],10);
			memcpy(achDueDate, &pchRawData[26], 6);
			strcat(achRefNo2, achDueDate);
			memcpy(achAmount,&pchRawData[32],9);

			// Update Tax ID screen
			sRet = DATA_FW.PutScreen(achTaxId,strlen(achTaxId),achTaxIdScr, SCREEN_SECTION);

			// Update Customer No screen
			sRet = DATA_FW.PutScreen(achRefNo1,strlen(achRefNo1),achCustNoScr, SCREEN_SECTION);
		
			// Update Reference No screen
			sRet = DATA_FW.PutScreen(achRefNo2,strlen(achRefNo2),achRefNoScr, SCREEN_SECTION);

			// Format Amount with 2 decimal

			memset(achBuf1,0,sizeof(achBuf1));
			memset(achBuf2,0,sizeof(achBuf2));
			length = strlen(achAmount) - 2;
			if(length > 0)
			{
				memcpy(achBuf1,&achAmount[0],length);
				memcpy(achBuf2,&achAmount[length],2);
				memset(achTmp,0,sizeof(achTmp));
				sprintf(achTmp,"%d.%02d",atoi(achBuf1),atoi(achBuf2));
			}
			else
				sprintf(achTmp,"0.%02d",atoi(achAmount));

			sRet = DATA_FW.PutScreen(achTmp,strlen(achTmp),achAmountScr, SCREEN_SECTION);

			/*
				Add 3 leading zeroes to Amount
			*/
			memset(achTmp, 0, sizeof(achTmp));
			strcpy(achTmp, "000");
			strcat(achTmp, achAmount);
			strcpy(achAmount, achTmp);
			achBarcodeType[0] = '1';
			TrcWritef(TRC_FUNC, " Set achBarcodeType =%c",achBarcodeType[0]);
			break;
	}

	/*
		Now, set the Amount Buffer, Buffer B and Buffer C
	*/

	//	Clear Buffer B & C
/*
	TrcWritef(TRC_FUNC, "Farm rt =%d",sRc);

	if(sRc == -5)
	{
		sRc = 1;
		TrcWritef(TRC_FUNC, "Farm rt2 =%d",sRc);
		return (sRc);
	}
*/	
	memset(achBufferB, ' ', sizeof(achBufferB));
	memset(achBufferC, ' ', sizeof(achBufferC));
	achBufferB[32] = 0;
	achBufferC[32] = 0;

	memcpy(achBufferB, achTaxId, strlen(achTaxId));
	
	if(achBarcodeType[0] == '1')
	{

		length = strlen(achRefNo1);
		TrcWritef(TRC_FUNC, "achRefNo1 length = %d",length);
		
		if ((length + (sRefNo1Pos + 2)) > GEN_BUFF_LEN)
		{
			sPart1Len = GEN_BUFF_LEN - (sRefNo1Pos+2);
			sPart2Len = length - sPart1Len;
			
			TrcWritef(TRC_FUNC, "achRefNo1 sPart1Len = %d sPart2Len = %d",
				sPart1Len,sPart2Len);

			memcpy(&achBufferB[sRefNo1Pos+2], achRefNo1, sPart1Len);
			memcpy(achBufferC, &achRefNo1[sPart1Len], sPart2Len);
		}
		else
		{
			memcpy(&achBufferB[sRefNo1Pos+2], achRefNo1, length);
		}
				
		memcpy(&achBufferC[sRefNo2Pos+2], achRefNo2, strlen(achRefNo2));
		
		if (fBarcodeType)
		{
				
				achBufferC[sBarcodeTypePos] = achBarcodeType[0];
		}
		
		TrcWritef(TRC_FUNC, "Type 1 TaxId = \"%s\", ReferenceNo1 = \"%s\", ReferenceNo2 = \"%s\", Amount = \"%s\"\nBuffer B = \"%s\", Buffer C = \"%s\""
							, achTaxId,achRefNo1,achRefNo2,achAmount, achBufferB, achBufferC);
	}
	else if (achBarcodeType[0] == '2')
	{
		length = strlen(achRefNo1);
		
		if ((length + sRefNo1Pos) > GEN_BUFF_LEN)
		{
			sPart1Len = GEN_BUFF_LEN - sRefNo1Pos;
			sPart2Len = length - sPart1Len;
			memcpy(&achBufferB[sRefNo1Pos], achRefNo1, sPart1Len);
			memcpy(achBufferC, &achRefNo1[sPart1Len], sPart2Len);
		}
		else
		{
			memcpy(&achBufferB[sRefNo1Pos], achRefNo1, length);
		}
				
		memcpy(&achBufferC[sRefNo2Pos], achRefNo2, strlen(achRefNo2));
		
		if (fBarcodeType)
		{
				
				achBufferC[sBarcodeTypePos] = achBarcodeType[0];
		}

		
		TrcWritef(TRC_FUNC, "Type 2  TaxId = \"%s\", ReferenceNo1 = \"%s\", ReferenceNo2 = \"%s\", Amount = \"%s\"\nBuffer B = \"%s\", Buffer C = \"%s\""
							, achTaxId,achRefNo1,achRefNo2,achAmount, achBufferB, achBufferC);


	}
	else 
	{
		
		length = strlen(achRefNo1);
		
		if ((length + sRefNo1Pos) > GEN_BUFF_LEN)
		{
			sPart1Len = GEN_BUFF_LEN - sRefNo1Pos;
			sPart2Len = length - sPart1Len;
			memcpy(&achBufferB[sRefNo1Pos], achRefNo1, sPart1Len);
			memcpy(achBufferC, &achRefNo1[sPart1Len], sPart2Len);
		}
		else
		{
			memcpy(&achBufferB[sRefNo1Pos], achRefNo1, length);
		}
				
		memcpy(&achBufferC[sRefNo2Pos], achRefNo2, strlen(achRefNo2));
		TrcWritef(TRC_FUNC,"sBarcodeTypePos = %d",sBarcodeTypePos);
		if (fBarcodeType)
		{
				achBufferC[sBarcodeTypePos] = achBarcodeType[0];
		}

		
		TrcWritef(TRC_FUNC, "Type 3 TaxId = [%s], ReferenceNo1 = [%s], ReferenceNo2 = [%s], Amount = [%s]\nBuffer B = [%s], Buffer C = [%s]"
							, achTaxId,achRefNo1,achRefNo2,achAmount, achBufferB, achBufferC);

	}

	if(fSetAmountBuffer)//if there is amount
	{
		sRet = VAR_FW.SetVar(DCVAR_AMOUNT_BUF, achAmount);
	}
	
	sRet = VAR_FW.SetVar(DCVAR_GEN_B_BUF, achBufferB);
	TrcWritef(TRC_FUNC,"Set DCVAR_GEN_B_BUF = %d",sRet);
	sRet = VAR_FW.SetVar(DCVAR_GEN_C_BUF, achBufferC);
	TrcWritef(TRC_FUNC,"Set DCVAR_GEN_C_BUF = %d",sRet); //

	
	TrcWritef(TRC_FUNC,"< dcBarcode::FormatBarcode -> return %d", sRc);
	return sRc;
}


