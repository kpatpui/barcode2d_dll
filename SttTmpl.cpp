/*
    I N C L U D E S
*/
#include <stdio.h>
#include <stdlib.h>

#include "SttTmpl.hpp"
#include "trcerr_e.h"


/*
    S T A T I C   G L O B A L   V A R I A B L E S
*/
static CHAR szModG[] = "$MOD$ 150716 1000 DCBARCODE.DLL";
static CHAR szSegG[] = "$SEG$ 150716 1000 DCBARCODE.CPP";


/*
    I M P L E M E N T A T I O N
    ===========================

    class tmplTransactionFW
*/
tmplTransactionFW *tmplTransactionFW::pThisM = 0;


tmplTransactionFW::tmplTransactionFW()
{

    TrcErrInit(TRC_MODID_STTTMPL, szModG);
    TrcWritef(TRC_FUNC, "> tmplTransactionFW::tmplTransactionFW");

    FrmSetName(CCTAFW);
	FrmRegisterForEvents(CC_JRN_FW);
    pThisM = this;

    TrcWritef(TRC_FUNC, "< tmplTransactionFW::tmplTransactionFW");
}


tmplTransactionFW &tmplTransactionFW::Current()
{
    return (*pThisM);
}


SHORT tmplTransactionFW::AddStep(
        Step *pcoStep)
{
    SHORT sRc = CCFRMW_RC_OK;

    TrcWritef(TRC_FUNC, "> tmplTransactionFW::AddStep(%s)", pcoStep->FuncName());

    coaStepsM.Add(pcoStep);

    TrcWritef(TRC_FUNC, "< tmplTransactionFW::AddStep -> %d", sRc);
    return (sRc);
}


SHORT tmplTransactionFW::OnFrmEvent(
        PCHAR szSender,
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    SHORT sRc;
	CCString strBuf;
	CHAR chData[1024];

    TrcWritef(TRC_INFO, "> tmplTransactionFW::OnFrmEvent %d from %s",
                                                        sEventId, szSender);
   
	
	if (strcmp(szSender,CC_JRN_FW)==0)
	{
		TrcWritef(TRC_INFO, "> I got the event sEventId = %d" ,sEventId); 
		
		if (sEventId == 1)
		{
			TrcWritef(TRC_INFO, "> Journal Data = %s" ,(PCHAR)pData); 

			sprintf(chData,"%s",(CHAR*)pData);
			TrcWritef (TRC_INFO,"> Journal Data: %s", chData);
			
			strBuf = chData;

			if ((strBuf.includes("GO IN SERVICE COMMAND")) || ((strBuf.includes("Go in service command")))){
				
				TrcWritef(TRC_INFO, "Found event GO IN SERVICE COMMAND");
				
				
				CCBcrFW	* barcodeFwM; 
				barcodeFwM = new CCBcrFW;
				SHORT barcodeStatus;
				barcodeStatus = barcodeFwM->GetbStatus();
				TrcWritef(TRC_FUNC, "> BARCODE STATUS = (%d)",barcodeStatus);
				if(barcodeStatus == CCBCRFW_NOT_OPERATIONAL)
				{
					APPL_FW.TransactionHandlerRequest("T2HBarcodeError");
					journalFwM.Write(DCJOU_BCR_OFFLINE);
					TrcWritef(TRC_FUNC, "> BARCODE DEVICE ERROR = (%d)",sRc);
						
				}
				else
				{
					APPL_FW.TransactionHandlerRequest("T2HBarcodeGood");	
					TrcWritef(TRC_FUNC, "> BARCODE DEVICE OK = (%d)",sRc);
				}
				//Check Alarm Status
				CCSelFW		SEL_FW;
				SEL_STATUS pStatus;

                SEL_FW.GetStatus( &pStatus );
				
				TrcWritef(TRC_INFO, "SEL_FW GetStatus INPUT1[0x%.2x],INPUT2[0x%.2x],INPUT3[0x%.2x],INPUT4[0x%.2x]",pStatus.ausSensors[10]
				,pStatus.ausSensors[11],pStatus.ausSensors[12],pStatus.ausSensors[13]);

				if((pStatus.ausSensors[13] == CCSELFW_SIU_ON))
				{
					sRc = APPL_FW.TransactionHandlerRequest("T2HUnsolicitedAlarmError");
					journalFwM.Write(DCJOU_ALARM_OFFLINE);
					TrcWritef(TRC_INFO, "> Sent T2HUnsolicitedAlarmError");// 
				}
				//End Get Alarm Status
			}
			else if(strBuf.includes("Transaction start"))
			{
				//Check EPP Status
				PIN_STATUS PinStatus;
				CCEppFW	*	pEppG = NULL;
				pEppG = new CCEppFW;
				memset (&PinStatus, 0x00, sizeof(PinStatus));
				sRc = pEppG->InfCmdStatus(&PinStatus);
				TrcWritef(TRC_INFO, "> EPP InfCmdStatus returns %d fwDevice= %d", sRc,PinStatus.fwDevice);
				if (sRc == CCEPPFW_OK)
				{
					if(PinStatus.fwDevice != CCEPPFW_PIN_DEVONLINE)
					{
						sRc = APPL_FW.TransactionHandlerRequest("T2HUnsolicitedEDM");
						TrcWritef(TRC_INFO, "> Sent T2HUnsolicitedEDM when device offline"); 
					}
				}

				//Check EJ Status
				SHORT  sRc = 0;
				USHORT usStatus;
				USHORT usSupplyState;
				CCFRMW_FRMW_ERROR_EVENT_DATA evtData;
				
				NdcDdcDeviceStatusFW DEVSTA_FW;
				sRc = DEVSTA_FW.GetStatus(CC_JRN_FW, &usStatus);
				TrcWritef(TRC_INFO, "GetStatus returned with sRc = %d, usStatus = %d", sRc, usStatus);

				if (usStatus == DCDEVSTA_FATAL) // Device status fatal
				{
					sRc = APPL_FW.TransactionHandlerRequest("T2HUnsolicitedElectronicJournal");
					TrcWritef(TRC_INFO, "> Sent T2HUnsolicitedElectronicJournal EJ error"); 
				}

				sRc = DEVSTA_FW.GetError(CC_JRN_FW, &evtData);
				TrcWritef(TRC_INFO, "GetError returned with sRc = %d\nevtData.ulStClass = %lu, evtData.ulStCode = %lu, evtDta.ulStWarn = %lu", sRc, evtData.ulStClass, evtData.ulStCode, evtData.ulStWarn);

				sRc = DEVSTA_FW.GetSupplyState(CC_JRN_FW, &usSupplyState);
				TrcWritef(TRC_INFO, "GetSupplyState returned with sRc = %d, usSupplyState = %d", sRc, usSupplyState);
				
				/*//Check Alarm Status
				CCSelFW		SEL_FW;
				SEL_STATUS pStatus;

                SEL_FW.GetStatus( &pStatus );
				
				TrcWritef(TRC_INFO, "SEL_FW GetStatus INPUT1[0x%.2x],INPUT2[0x%.2x],INPUT3[0x%.2x],INPUT4[0x%.2x]",pStatus.ausSensors[10]
				,pStatus.ausSensors[11],pStatus.ausSensors[12],pStatus.ausSensors[13]);

				if((pStatus.ausSensors[13] == CCSELFW_SIU_ON))
				{
					sRc = APPL_FW.TransactionHandlerRequest("T2HUnsolicitedAlarmError");
					TrcWritef(TRC_INFO, "> Sent T2HUnsolicitedAlarmError");// 
				}*/
			}
		
		}

	}
	
	/*
        Route the event to the base class
    */
    sRc = OnFrmEventBaseClass(szSender, sEventId, pData, sDataLen);

    TrcWritef(TRC_INFO, "< tmplTransactionFW::OnFrmEvent");
    return(CCFRMW_RC_OK);  

}


SHORT tmplTransactionFW::OnFrmRequest(
        SHORT sMethodId,
        VOID *pData1,
        SHORT sDataLen1,
        VOID *pData2,
        SHORT sDataLen2,
        VOID *pData3,
        SHORT sDataLen3,
        ULONG ulTimeOut)
{
    SHORT sRc;

    TrcWritef(TRC_INFO, "> tmplTransactionFW::OnFrmRequest %d", sMethodId);

    switch(sMethodId)
    {
		case CCTAFW_FUNC_INIT_RESOURCES:
        
			sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            if ((sRc == CCFRMW_RC_FUNCTION_NOT_SUPPORTED) ||
                (sRc == CCFRMW_RC_OK))
            {
                InitStepList();
            }
            break;

        case CCTAFW_FUNC_PROCESS_STEP:
            sRc = ProcessStep((PCHAR)pData1, (PCHAR)pData2, (PCHAR)pData3);
            break;

        default:
            sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            break;

    }

    TrcWritef(TRC_INFO, "< tmplTransactionFW::OnFrmRequest -> %d", sRc);
    return(sRc);
}



SHORT tmplTransactionFW::ProcessStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    int iIdx = 0;
    int iNumSteps = coaStepsM.GetSize();
    BOOL fFound = FALSE;
    unsigned int uiStepLen = strlen(szStepFunc);
    Step *pcoStep;
    SHORT sRc;

    TrcWritef(TRC_FUNC, "> tmplTransactionFW::ProcessStep(%s, %s, %s)",
                                pchSectNextStep, szStepFunc, szStepParams);

    while (!fFound && (iIdx < iNumSteps))
    {
        pcoStep = (Step *)coaStepsM.GetAt(iIdx);
        if ((uiStepLen == strlen(pcoStep->FuncName())) &&
            (strcmp(szStepFunc, pcoStep->FuncName()) == 0))
        {
            fFound = TRUE;

            TrcWritef(TRC_INFO, "Found index %d, calling Process", iIdx);

            sRc = pcoStep->Process(pchSectNextStep, szStepParams);
        }
        else
        {
            iIdx++;
        }
    }

    if (!fFound)
    {
        TrcWritef(TRC_INFO, "Not found, calling base class");

        sRc = OnFrmRequestBaseClass(CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1, 0);
    }
/* KARLI start 970602 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
    /*
        Now this is very specific:
        If the next state is state 000 or no next state given we go to the
        TRAN_END step(default close) to completely finish the transaction.
    */
    if (sRc == CCTAFW_RC_JUMP_LABEL)
    {
        if (!*pchSectNextStep || !strcmp(pchSectNextStep, "000"))
        {
            strcpy(pchSectNextStep, "TRAN_END");    // For now !!!!!
        }
    }
/* KARLI end   970602 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

    TrcWritef(TRC_FUNC, "< tmplTransactionFW::ProcessStep -> %d, next %s",
                                                    sRc, pchSectNextStep);
    return(sRc);
}


/*
    I M P L E M E N T A T I O N
    ===========================

    class Step
*/
SHORT Step::FrmSendEvent(
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    return (tmplTransactionFW::Current().FrmSendEvent(sEventId,
                                                            pData, sDataLen));
}


SHORT Step::FrmSendEventSync(
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    return (tmplTransactionFW::Current().FrmSendEventSync(sEventId,
                                                            pData, sDataLen));
}


SHORT Step::FrmEnterCritSec()
{
    return (tmplTransactionFW::Current().FrmEnterCritSec());
}


SHORT Step::FrmExitCritSec()
{
    return (tmplTransactionFW::Current().FrmExitCritSec());
}


SHORT Step::ProcessStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (tmplTransactionFW::Current().FrmResolve(CCTAFW,
                                CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1));
}


SHORT Step::ProcessBaseStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (tmplTransactionFW::Current().OnFrmRequestBaseClass(
                                CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1, 0));

}


SHORT Step::ProcessStepAsync(
        CCFRMW_JOB_STATUS *pJobState,
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (tmplTransactionFW::Current().FrmAsyncResolve(CCTAFW,
                                CCTAFW_FUNC_PROCESS_STEP, pJobState,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1));
}




BOOL Step::FrmCheckAsyncCompletion(
        CCFRMW_JOB_STATUS *pJobState,
        ULONG ulTimeOut)
{
    return (tmplTransactionFW::Current().FrmCheckAsyncCompletion(
                                                    pJobState, ulTimeOut));
}

/*
    I M P L E M E N T A T I O N
    ===========================

    CreateFrameWorkInstance
*/
SHORT _Export CreateFrameWorkInstance(
        PCHAR szFrameWorkName,
        CCFrameWork **ppFW)
{
    SHORT sRc;

    if (stricmp(szFrameWorkName, CCTAFW) == 0)
    {   /* create an instance of tmplTransactionFW */
        *ppFW = (CCFrameWork *)new tmplTransactionFW;

        sRc = 0;
    }
    else
    {
        sRc = -1;
    }

    return(sRc);
}

