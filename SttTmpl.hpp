#ifndef DCSTTFW_HPP
#define DCSTTFW_HPP
/*
    I N C L U D E S
*/
#include "ccFrmw.hpp"
#include "collect.hpp"
#include "cctrnact.xpp"
#include "ccVarFw.xpp"
#include "CCEppFW.xpp"
#include "ccApplic.xpp"
#include "ccDialog.xpp"
#include "ccJourna.xpp"
#include "ccDataFW.xpp"
#include "cccinfw.xpp"
#include "ccIniPar.hpp"
#include "ccUti_e.h"
#include "cctimdat.hpp"
#include "dcjouent.hpp"
#include "dcdatafw.xpp"
#include "dcDialog.xpp"
#include "dcStepFW.xpp"
#include "dcApplFW.xpp"
#include "winfp.xpp"
#include "ccBCRfw.xpp"
#include "dcdevsta.xpp"
#include "CCSELFW.XPP"
/*
    D E F I N E S
*/
#define TRC_MODID_STTTMPL       7002
#define TRC_FUNC                10
#define TRC_INFO                11
#define TRC_DETAIL              12


#define THIS_STEP_FW        tmplTransactionFW::Current()
#define VAR_FW              tmplTransactionFW::Current().varFwM
#define DIALOG_FW          	tmplTransactionFW::Current().dlgFwM
#define DATA_FW				tmplTransactionFW::Current().dataFwM
#define STEP_FW             tmplTransactionFW::Current().stepFwM
#define PCMOS_FW            tmplTransactionFW::Current().pcmosFwM
#define FP_FW               tmplTransactionFW::Current().FpFwM
#define APPL_FW             tmplTransactionFW::Current().applFwM

#define DCBARCODE_SECTION			"BARCODE"
#define DCBARCODE_CUSTOMER			"CUSTOMER"
#define DCBARCODE_MAX_TIMEOUT		"MAX_TIMEOUT"
/*
	Barcode Screens
*/
#define BARCODE_SCR_TAX_ID			"TAX_ID_SCREEN"
#define BARCODE_SCR_CUST_NO			"CUSTOMER_NO_SCREEN"
#define BARCODE_SCR_REF_NO			"REF_NO_SCREEN"
#define BARCODE_SCR_AMOUNT			"AMOUNT_SCREEN"

// return values from the member functions
#define	MMBARCODEFW_RC_OK                   0
#define	MMBARCODEFW_RC_ERROR                -1
#define MMBARCODEFW_RC_TIMEOUT              -2
#define MMBARCODEFW_RC_BUFFER_TOO_SMALL     -3
#define MMBARCODEFW_RC_CANCELLED            -4

// define EJ top msg
#define DCJOU_ALARM_OFFLINE				9606
#define DCJOU_BCR_OFFLINE				9607

#ifdef CC_W32
#pragma pack(push, 4)
#else
#pragma pack(4)
#endif


class Step : public CCObject
{
    public:
        SHORT FrmSendEvent(SHORT, PVOID = 0, SHORT = 0);
        SHORT FrmSendEventSync(SHORT, PVOID = 0, SHORT = 0);

        SHORT FrmEnterCritSec();
        SHORT FrmExitCritSec();

        SHORT ProcessStep(PCHAR pchSectNextStep, PCHAR szStepFunc,
                                                        PCHAR szStepParams);
        SHORT ProcessBaseStep(PCHAR pchSectNextStep, PCHAR szStepFunc,
                                                        PCHAR szStepParams);

        SHORT ProcessStepAsync(CCFRMW_JOB_STATUS *pJobState,
                PCHAR pchSectNextStep, PCHAR szStepFunc, PCHAR szStepParams);
        BOOL FrmCheckAsyncCompletion(CCFRMW_JOB_STATUS *, ULONG ulTimeOut);
        /*
            Following methods have to be implemented
        */
        virtual SHORT Process(PCHAR pchSectNextStep, PCHAR szStepParams) = 0;

        virtual PCHAR FuncName() = 0;
};


class tmplTransactionFW : public CCFrameWork
{
    private:
        static tmplTransactionFW *pThisM;

        CCObArray coaStepsM;

        SHORT AddStep(Step *pStep);

        VOID InitStepList();    // Located in dcStt.cpp



    public:
        tmplTransactionFW();

        static tmplTransactionFW &Current();

        virtual SHORT OnFrmEvent(PCHAR, SHORT, VOID *, SHORT);

        virtual SHORT OnFrmRequest(SHORT, VOID *, SHORT,
                                        VOID *, SHORT, VOID *, SHORT, ULONG);

        SHORT ProcessStep(PCHAR, PCHAR, PCHAR);

		
		CCJournalFW journalFwM;
		CCVarFW		varFwM;
		dcDataFW	dataFwM;
		CCCmos		pcmosFwM;
		WinFpFW     FpFwM; 
		
        NdcDdcDialogFW			dlgFwM;
        NdcDdcStepFW			stepFwM;
		CCApplicationFW         applFwM;
		


};
class ParameterList
{
    private:
        CCObArray coaParamM;

    public:
        ParameterList(
            PCHAR szParameter, 
            CHAR chSeparator = ',',
            BOOL fRemoveSpaces = FALSE);
        ~ParameterList();

        PCHAR Param(int) const;
        INT   Size () const { return coaParamM.GetSize(); };
};

class dcCardRead : public Step
{
    public:
        virtual SHORT Process(          // Returns JUMP_ON_RC index
                PCHAR pchSectNextStep,  // IN: Section name
                PCHAR szStepParams);    // IN: Step parameters

        
		virtual PCHAR FuncName() { return (DCSTEP_CARD_READ); };
};
class dcBarcode : public Step
{
    public:
        virtual SHORT Process(          // Returns JUMP_ON_RC index
                PCHAR pchSectNextStep,  // IN: Section name
                PCHAR szStepParams);    // IN: Step parameters
		
				SHORT FormatBarcode(SHORT sBarcodeType, PCHAR pchRawData);
				VOID  UpdateCounter(VOID);
				SHORT sTxnTypeM;
				SHORT sFlagTypeM;
        virtual PCHAR FuncName() { return ("DC_READ_BCR"); };

};


class PrtParam
{
    private:
        CCObArray coaParamM;

    public:
        PrtParam(PCHAR);
        ~PrtParam();

        PCHAR Param(int) const;
		SHORT GetSize();
};

class CCStringObject : public CCObject , public CCString
{
    public:
        CCStringObject() : CCString() {};
        CCStringObject(PCHAR szTxt) : CCString(szTxt) {};
        CCStringObject(PCHAR szTxt, SHORT sLen) : CCString(szTxt, sLen) {};
};

#ifdef CC_W32
#pragma pack(pop)
#else
#pragma pack()
#endif

#endif
